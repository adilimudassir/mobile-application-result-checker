-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2019 at 11:09 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `result`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `classes` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_code` varchar(32) DEFAULT NULL,
  `course_name` varchar(80) DEFAULT NULL,
  `credit_unit` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_code`, `course_name`, `credit_unit`) VALUES
(2, 'COM211', 'COMPUTER PROGRAMMING USING OOVB', '4'),
(3, 'COM212', 'INTRODUCTION TO SYSTEM PROGRAMMI', '4'),
(4, 'COM213', 'COMMERCIAL PROGRAMMING LANGUAGE ', '4'),
(5, 'COM214', 'FILE ORGANIZATION AND MANAGEMENT', '3'),
(6, 'COM215', 'COMPUTER PACKAGES II', '4'),
(7, 'COM216', 'COMPUTER SYSTEM TROUBLESHOOTING ', '4'),
(8, 'GNS201', 'USE OF ENGLISH II', '2'),
(9, 'GNS218', 'RESEARCH METHODOLOGY', '2'),
(10, 'GNS216', 'ENTREPRENEURSHIP DEVELOPMENT', '3'),
(11, 'COM217', 'SIWES', '4'),
(12, 'COM218', 'OFFICE ICT MANAGEMENT', '2');

-- --------------------------------------------------------

--
-- Table structure for table `cummulative`
--

CREATE TABLE `cummulative` (
  `id` int(11) NOT NULL,
  `reg_no` varchar(32) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `course` varchar(60) DEFAULT NULL,
  `total_gps` double(11,0) DEFAULT NULL,
  `total_cus` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cummulative`
--

INSERT INTO `cummulative` (`id`, `reg_no`, `code`, `course`, `total_gps`, `total_cus`) VALUES
(1, '2013/ND/CPS/001', 'COM213', 'COMMERCIAL PROGRAMMING LANGUAGE ', 10, 4),
(2, '2013/ND/CPS/001', 'GNS201', 'USE OF ENGLISH II', 8, 2),
(3, '2013/ND/CPS/010', 'COM211', 'COMPUTER PROGRAMMING USING OOVB', 10, 4),
(4, '2013/ND/CPS/010', 'COM215', 'COMPUTER PACKAGES II', 14, 4),
(5, '2013/ND/CPS/003', 'COM213', 'COMMERCIAL PROGRAMMING LANGUAGE ', 12, 4),
(6, '2013/ND/CPS/003', 'COM217', 'SIWES', 14, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cummulative2`
--

CREATE TABLE `cummulative2` (
  `id` int(11) NOT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `course` varchar(50) DEFAULT NULL,
  `total_gps` double(11,0) DEFAULT NULL,
  `total_cus` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `first_semester`
--

CREATE TABLE `first_semester` (
  `id` int(11) NOT NULL,
  `reg_no` varchar(32) DEFAULT NULL,
  `semester` varchar(32) DEFAULT NULL,
  `session` varchar(32) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `course` varchar(60) DEFAULT NULL,
  `cu` int(11) DEFAULT NULL,
  `gp` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `first_semester`
--

INSERT INTO `first_semester` (`id`, `reg_no`, `semester`, `session`, `code`, `course`, `cu`, `gp`) VALUES
(1, '2013/ND/CPS/001', '1ST SEMESTER', '2013/2014', 'COM213', 'COMMERCIAL PROGRAMMING LANGUAGE ', 4, 10),
(2, '2013/ND/CPS/001', '1ST SEMESTER', '2013/2014', 'GNS201', 'USE OF ENGLISH II', 2, 8),
(3, '2013/ND/CPS/010', '1ST SEMESTER', '2013/2014', 'COM211', 'COMPUTER PROGRAMMING USING OOVB', 4, 10),
(4, '2013/ND/CPS/010', '1ST SEMESTER', '2013/2014', 'COM215', 'COMPUTER PACKAGES II', 4, 14),
(5, '2013/ND/CPS/003', '1ST SEMESTER', '2013/2014', 'COM213', 'COMMERCIAL PROGRAMMING LANGUAGE ', 4, 12),
(6, '2013/ND/CPS/003', '1ST SEMESTER', '2013/2014', 'COM217', 'SIWES', 4, 14);

-- --------------------------------------------------------

--
-- Table structure for table `grading`
--

CREATE TABLE `grading` (
  `id` int(11) NOT NULL,
  `gfrom` varchar(11) DEFAULT NULL,
  `gto` varchar(11) DEFAULT NULL,
  `grade` varchar(11) DEFAULT NULL,
  `grade_value` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grading`
--

INSERT INTO `grading` (`id`, `gfrom`, `gto`, `grade`, `grade_value`) VALUES
(21, '0', '39', 'F', '0'),
(22, '40', '44', 'E', '1'),
(23, '45', '49', 'D', '2'),
(24, '50', '54', 'CD', '3'),
(25, '55', '59', 'C', '4'),
(26, '60', '64', 'BC', '5'),
(27, '65', '69', 'C', '6'),
(28, '70', '74', 'AB', '7'),
(29, '75', '100', 'A', '8');

-- --------------------------------------------------------

--
-- Table structure for table `lecturers`
--

CREATE TABLE `lecturers` (
  `id` int(11) NOT NULL,
  `lecturer` varchar(60) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lecturers`
--

INSERT INTO `lecturers` (`id`, `lecturer`, `username`, `password`) VALUES
(1, 'Mal. Bello Hassan Alustapha', 'bhalmustapha', '123456'),
(2, 'Mal. Tambari Aliyu', 'tambari', '123456'),
(3, 'Mal Shittu Usman', 'shittu', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `id` int(11) NOT NULL,
  `reg_no` varchar(35) DEFAULT NULL,
  `semester` varchar(35) DEFAULT NULL,
  `session` varchar(35) DEFAULT NULL,
  `course_code` varchar(30) DEFAULT NULL,
  `cu` int(11) DEFAULT NULL,
  `course_name` varchar(35) DEFAULT NULL,
  `scores` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`id`, `reg_no`, `semester`, `session`, `course_code`, `cu`, `course_name`, `scores`) VALUES
(1, '2013/ND/CPS/001', '1ST SEMESTER', '2013/2014', 'COM213', 4, 'COMMERCIAL PROGRAMMING LANGUAGE ', 50),
(2, '2013/ND/CPS/001', '1ST SEMESTER', '2013/2014', 'GNS201', 2, 'USE OF ENGLISH II', 80),
(3, '2013/ND/CPS/010', '1ST SEMESTER', '2013/2014', 'COM211', 4, 'COMPUTER PROGRAMMING USING OOVB', 55),
(4, '2013/ND/CPS/010', '1ST SEMESTER', '2013/2014', 'COM215', 4, 'COMPUTER PACKAGES II', 77),
(5, '2013/ND/CPS/003', '1ST SEMESTER', '2013/2014', 'COM213', 4, 'COMMERCIAL PROGRAMMING LANGUAGE ', 66),
(6, '2013/ND/CPS/003', '1ST SEMESTER', '2013/2014', 'COM217', 4, 'SIWES', 79);

--
-- Triggers `scores`
--
DELIMITER $$
CREATE TRIGGER `create_result` AFTER INSERT ON `scores` FOR EACH ROW BEGIN

DECLARE which_semester VARCHAR(32);
DECLARE which_session VARCHAR(32);
DECLARE course_cod VARCHAR(32);
DECLARE courses VARCHAR(60);
DECLARE cus DOUBLE;
DECLARE course_scores DOUBLE;
DECLARE graded_value DOUBLE;
DECLARE grade_point DOUBLE;
DECLARE course_flag INT;
DECLARE cumm_gp DOUBLE;
DECLARE cumm_cu DOUBLE;
DECLARE updated_gp DOUBLE;
DECLARE updated_cus DOUBLE;
DECLARE regno VARCHAR(32);

SET regno = NEW.reg_no;
SET which_semester = NEW.semester;
SET which_session = NEW.session;
SET course_cod = NEW.course_code;
SET courses= NEW.course_name;
SET cus =NEW.cu;
SET course_scores =NEW.scores;
SET graded_value =0;
SET grade_point =0;
SET cumm_gp=0;
SET updated_cus=0;

IF which_semester='4TH SEMESTER' THEN

      SET graded_value=(SELECT grade_value FROM grading WHERE gfrom<=course_scores AND gto>=course_scores);
             IF graded_value>0 THEN
                SET  grade_point=graded_value*cus;
                    INSERT INTO fourth_semester (reg_no,semester,session,code,course,cu,gp) VALUES(regno,which_semester,which_session,course_cod,courses,cus,grade_point);
             ELSE
                SET  grade_point=0*cus;
                    INSERT INTO fourth_semester (reg_no,semester,session,code,course,cu,gp) VALUES(regno,which_semester,which_session,course_cod,courses,cus,grade_point);
             END IF;

     SET course_flag=(SELECT COUNT(code)  FROM cummulative2 WHERE code = course_cod AND reg_no=regno);
           IF course_flag>0 THEN
              SET cumm_gp=(SELECT total_gps FROM cummulative2 WHERE code = course_cod  AND reg_no=regno);
              SET cumm_cu=(SELECT total_cus FROM cummulative2 WHERE code = course_cod  AND reg_no=regno);
              SET updated_gp= cumm_gp+grade_point;
              SET updated_cus= cumm_cu+cus;
              UPDATE cummulative2 SET total_gps= updated_gp,total_cus=updated_cus WHERE code= course_cod  AND reg_no=regno;
          ELSE
              INSERT INTO cummulative2 (reg_no,code,course,total_gps,total_cus) VALUES(regno,course_cod,courses,grade_point,cus);
          END IF; 

ELSEIF which_semester='3RD SEMESTER' THEN

      SET graded_value=(SELECT grade_value FROM grading WHERE gfrom<=course_scores AND gto>=course_scores);
             IF graded_value>0 THEN
                SET  grade_point=graded_value*cus;
                    INSERT INTO third_semester (reg_no,semester,session,code,course,cu,gp) VALUES(regno,which_semester,which_session,course_cod,courses,cus,grade_point);
             ELSE
                SET  grade_point=0*cus;
                    INSERT INTO third_semester (reg_no,semester,session,code,course,cu,gp) VALUES(regno,which_semester,which_session,course_cod,courses,cus,grade_point);
             END IF;

     SET course_flag=(SELECT COUNT(code)  FROM cummulative2 WHERE code = course_cod AND reg_no=regno);
           IF course_flag>0 THEN
              SET cumm_gp=(SELECT total_gps FROM cummulative2 WHERE code = course_cod  AND reg_no=regno);
              SET cumm_cu=(SELECT total_cus FROM cummulative2 WHERE code = course_cod  AND reg_no=regno);
              SET updated_gp= cumm_gp+grade_point;
              SET updated_cus= cumm_cu+cus;
              UPDATE cummulative2 SET total_gps= updated_gp,total_cus=updated_cus WHERE code= course_cod  AND reg_no=regno;
          ELSE
              INSERT INTO cummulative2 (reg_no,code,course,total_gps,total_cus) VALUES(regno,course_cod,courses,grade_point,cus);
          END IF; 

ELSEIF which_semester='1ST SEMESTER' THEN

      SET graded_value=(SELECT grade_value FROM grading WHERE gfrom<=course_scores AND gto>=course_scores);
             IF graded_value>0 THEN
                SET  grade_point=graded_value*cus;
                    INSERT INTO first_semester (reg_no,semester,session,code,course,cu,gp) VALUES(regno,which_semester,which_session,course_cod,courses,cus,grade_point);
             ELSE
                SET  grade_point=0*cus;
                    INSERT INTO first_semester (reg_no,semester,session,code,course,cu,gp) VALUES(regno,which_semester,which_session,course_cod,courses,cus,grade_point);
             END IF;

     SET course_flag=(SELECT COUNT(code)  FROM cummulative WHERE code = course_cod AND reg_no=regno);
           IF course_flag>0 THEN
              SET cumm_gp=(SELECT total_gps FROM cummulative WHERE code = course_cod  AND reg_no=regno);
              SET cumm_cu=(SELECT total_cus FROM cummulative WHERE code = course_cod  AND reg_no=regno);
              SET updated_gp= cumm_gp+grade_point;
              SET updated_cus= cumm_cu+cus;
              UPDATE cummulative SET total_gps= updated_gp,total_cus=updated_cus WHERE code= course_cod  AND reg_no=regno;
          ELSE
              INSERT INTO cummulative (reg_no,code,course,total_gps,total_cus) VALUES(regno,course_cod,courses,grade_point,cus);
          END IF; 

ELSEIF which_semester='2ND SEMESTER' THEN

      SET graded_value=(SELECT grade_value FROM grading WHERE gfrom<=course_scores AND gto>=course_scores);
             IF graded_value>0 THEN
                SET  grade_point=graded_value*cus;
                    INSERT INTO second_semester (reg_no,semester,session,code,course,cu,gp) VALUES(regno,which_semester,which_session,course_cod,courses,cus,grade_point);
             ELSE
                SET  grade_point=0*cus;
                    INSERT INTO second_semester (reg_no,semester,session,code,course,cu,gp) VALUES(regno,which_semester,which_session,course_cod,courses,cus,grade_point);
             END IF;

     SET course_flag=(SELECT COUNT(code)  FROM cummulative WHERE code = course_cod  AND reg_no=regno);
           IF course_flag>0 THEN
              SET cumm_gp=(SELECT total_gps FROM cummulative WHERE code = course_cod  AND reg_no=regno);
              SET cumm_cu=(SELECT total_cus FROM cummulative WHERE code = course_cod  AND reg_no=regno);
              SET updated_gp= cumm_gp+grade_point;
              SET updated_cus= cumm_cu+cus;
              UPDATE cummulative SET total_gps= updated_gp,total_cus=updated_cus WHERE code= course_cod  AND reg_no=regno;
          ELSE
              INSERT INTO cummulative (reg_no,code,course,total_gps,total_cus) VALUES(regno,course_cod,courses,grade_point,cus);
          END IF; 
       
END IF;






END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `second_semester`
--

CREATE TABLE `second_semester` (
  `id` int(11) NOT NULL,
  `reg_no` varchar(32) DEFAULT NULL,
  `semester` varchar(32) DEFAULT NULL,
  `session` varchar(32) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `course` varchar(60) DEFAULT NULL,
  `cu` int(11) DEFAULT NULL,
  `gp` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `session` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `session`) VALUES
(1, '2016/2017'),
(2, '2017/2018'),
(3, '2018/2019'),
(4, '2019/2020');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `student_name` varchar(32) DEFAULT NULL,
  `reg_no` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `student_name`, `reg_no`) VALUES
(186, 'MUSA MARIAM', '175493015'),
(187, 'MUSA BARIKISU', '175493018'),
(188, 'MOMOH SUNDAY ALEX', '175493021'),
(189, 'AINA OLUMIDE JOEL', '175493016'),
(190, 'AUDU SENUSI TIJANI', '175493034'),
(191, 'ABDULAZEEZ RUKAYAT', '175493099'),
(195, 'SULEIMAN JUMAI IZE', '175493065'),
(196, 'USMAN TIJANI', '175493007');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cummulative`
--
ALTER TABLE `cummulative`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cummulative2`
--
ALTER TABLE `cummulative2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `first_semester`
--
ALTER TABLE `first_semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grading`
--
ALTER TABLE `grading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecturers`
--
ALTER TABLE `lecturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `second_semester`
--
ALTER TABLE `second_semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `cummulative`
--
ALTER TABLE `cummulative`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cummulative2`
--
ALTER TABLE `cummulative2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `first_semester`
--
ALTER TABLE `first_semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `grading`
--
ALTER TABLE `grading`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `lecturers`
--
ALTER TABLE `lecturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `second_semester`
--
ALTER TABLE `second_semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
